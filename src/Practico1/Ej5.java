package Practico1;

import java.util.Scanner;


public class Ej5 {
	public static void main(String [] args){
		Integer a,b;
		Scanner input = new Scanner(System.in);
		System.out.println("Ingresa 2 numeros: \n");
		System.out.println("Numero 1: ");
		a = input.nextInt();
		
		System.out.println("Numero 2: ");
		b = input.nextInt();
		
		System.out.println((a % b == 0 ? "Son multiplos" : "No son multiplos"));

	}


}
