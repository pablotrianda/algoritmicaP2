package Practico1;

import java.util.Scanner;

public class Ej1 {
	public static void main(String [] args){
		Integer a,b;
		Scanner input = new Scanner(System.in);
		System.out.println("Ingresa 2 numeros: \n");
		System.out.println("Numero 1: ");
		a = input.nextInt();
		
		System.out.println("Numero 2: ");
		b = input.nextInt();
		
		suma(a,b);
		producto(a,b);
		diferencia(a,b);
		cociente(a,b);
	
	}

	private static void cociente(Integer a, Integer b) {
		System.out.println((b != 0 ? a / b : "Error"));
		
		
	}

	private static void diferencia(Integer a, Integer b) {
		System.out.println(a - b);
		
	}

	private static void producto(Integer a, Integer b) {
		System.out.println(a * b);
	}

	private static void suma(Integer a, Integer b) {
		System.out.println(a + b);
		
	}

	

}
