package Practico1;

import java.util.Scanner;

public class Ej3 {
	public static void main(String [] args){
		Integer a,b,c;
		Scanner input = new Scanner(System.in);
		System.out.println("Ingresa 3 numeros: \n");
		
		System.out.println("Numero 1: ");
		a = input.nextInt();
		
		System.out.println("Numero 2: ");
		b = input.nextInt();
		
		System.out.println("Numero 3: ");
		c = input.nextInt();
		
		suma(a,b,c);
		media(a,b,c);
		producto(a,b,c);
		minimo(a,b,c);
		maximo(a,b,c);
	}

	private static void minimo(Integer a, Integer b, Integer c) {
		if (a != b ||  b != c || a != c){
			if (a <= b && a <= c)
				System.out.printf("%d es el menor",a);
			else if (b <= a && b <= c)
				System.out.printf("%d es el menor",b);
			else
				System.out.printf("%d es el menor",c);
		}else
			System.out.println("Son iguales");
		System.out.println();
	}

	private static void maximo(Integer a, Integer b, Integer c) {
		if (a != b ||  b != c || a != c){
			if (a >= b && a >= c)
				System.out.printf("%d es el mayor",a);
			else if (b >= a && b >= c)
				System.out.printf("%d es el mayor",b);
			else
				System.out.printf("%d es el mayor",c);
		}else
			System.out.println("Son iguales");
		System.out.println();

	}

	private static void producto(Integer a, Integer b, Integer c) {
		System.out.println(a*b*c);
		
	}

	private static void media(Integer a, Integer b, Integer c) {
		System.out.println((a+b+c)/3);
		
	}

	private static void suma(Integer a, Integer b, Integer c) {
		System.out.println(a+b+c);
		
	}
}
