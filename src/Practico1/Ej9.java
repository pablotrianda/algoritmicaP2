package Practico1;

import java.util.Scanner;


public class Ej9 {
	public static void main(String [] args){
		Integer n;
		Boolean cont = true;
		Scanner input = new Scanner(System.in);
		
		do {
			System.out.println("Ingrese un numero de 5 digitos");
			n = input.nextInt();
			if (tieneCincoDigitos(n)){
				cont = false;
				EsPalindromo(n);
			}else{
				System.out.println("Este numero no tiene 5 digitos, \n");
			}
		} while (cont);
	}

	private static void EsPalindromo(Integer n) {
		//TODO verificar si es palindromo
	   System.err.println("Es palindormo");
	}

	private static boolean tieneCincoDigitos(Integer n) {
		Integer digitos = 0;    
	    while(n!=0){            
	      n = n/10;         
	      digitos++;          
	    }
		return (digitos == 5);
	}


}
