package Practico1;

public class Ej12 {

	public static void main(String[] args) {
		Integer[] numeros = new Integer[11];
		
		System.out.println("|Numero|	Cuandrado|	Cubo|");
		for (int i = 0;i < numeros.length; i++) {
			System.out.println("|------------------------------------|\n");
			System.out.printf("|  %d       	    %f       %f\n",i,Math.pow(i, 2),Math.pow(i, 3));
		}
		
	}
}

